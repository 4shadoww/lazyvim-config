return {
    -- add dracula
    {
        "Mofiqul/dracula.nvim",
        config = function()
            local dracula = require("dracula")
            dracula.setup({
                colors = {
                    nontext = "#6272a4",
                },
            })
        end,
    },

    -- Configure LazyVim to load dracula
    {
        "LazyVim/LazyVim",
        opts = {
            colorscheme = "dracula",
        },
    },

    -- add git blame
    {
        "FabijanZulj/blame.nvim",
        config = function()
            require("blame").setup()
        end,
    },

    {
        "nvim-telescope/telescope-file-browser.nvim",
        dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" },
    },
    {
        "nvim-orgmode/orgmode",
        event = "VeryLazy",
        ft = { "org" },
        config = function()
            -- Setup orgmode
            require("orgmode").setup({
                org_agenda_files = "~/org/**/*",
                org_default_notes_file = "~/org/refile.org",
            })
        end,
    },
    {
        "natecraddock/workspaces.nvim",
        config = function()
            require("workspaces").setup({
                path = vim.fn.stdpath("data") .. "/workspaces",
                cd_type = "local",
                sort = true,
                mru_sort = true,
                auto_open = false,
                notify_info = true,
                hooks = {
                    add = {},
                    remove = {},
                    rename = {},
                    open_pre = {},
                    open = {
                        "Telescope find_files",
                    },
                },
            })
            require("telescope").load_extension("workspaces")
        end,
    },
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        opts = {
            preset = "classic"
        },
    },
}
