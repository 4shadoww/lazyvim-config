-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here

local opt = vim.opt
opt.wrap = true
opt.colorcolumn = "80"
vim.g.autoformat = false
opt.clipboard = "unnamedplus"
vim.g.snacks_animate = false
vim.g.lazyvim_picker = "telescope"
vim.fn.setenv("JAVA_HOME", "/usr/lib/jvm/java-21-openjdk")
