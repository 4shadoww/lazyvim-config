-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

-- This file is automatically loaded by lazyvim.config.init
-- local vim = require("lazyvim.vim")

local map = vim.keymap.set

-- save file
map({ "n" }, "<leader>fs", "<cmd>w<cr><esc>", { desc = "Save file" })

-- Git blame
map({ "n" }, "<leader>gb", "<cmd>BlameToggle<cr><esc>", { desc = "Git blame" })

-- Telescope project
map({ "n" }, "<leader>p", "<cmd>Telescope workspaces<cr><esc>", { desc = "Telescope project" })

-- Telescope file browser
map(
    { "n" },
    "<leader>fj",
    "<cmd>Telescope file_browser path=%:p:h select_buffer=true<cr><esc>",
    { desc = "File browser" },
    { noremap = true }
)

-- Play macro
map("n", "<F9>", "@@")
